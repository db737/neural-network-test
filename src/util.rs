use rand;
use rand_distr::Distribution;
use std::fmt::{Display, Formatter, Result};
use std::ops::{Mul, Sub, SubAssign};

#[derive(Clone, Copy, Debug)]
pub struct SqMat<T, const N: usize> {
	// [row; N]
	pub elems: [[T; N]; N]
}

impl<T, const N: usize> SqMat<T, N> {
	pub fn new(elems: [[T; N]; N]) -> Self {
		Self {
			elems
		}
	}
}

impl<const N: usize> SqMat<f64, N> {
	pub fn zero() -> Self {
		Self {
			elems: [[0.0; N]; N]
		}
	}

	pub fn from_dist<D: Distribution<f64>>(dist: &D) -> Self {
		let mut rng = rand::thread_rng();
		let mut out = Self::zero();
		for i in 0 .. N {
			for j in 0 .. N {
				out.elems[i][j] = dist.sample(&mut rng)
			}
		}
		out
	}

	pub fn max_ratio(&self, diff: &Self) -> f64 {
		let mut max: f64 = 0.0;
		for i in 0 .. N {
			for j in 0 .. N {
				max = max.max(diff.elems[i][j].abs() / self.elems[i][j])
			}
		}
		max
	}

	pub fn transpose(&self) -> Self {
		let mut out = Self::zero();
		for i in 0 .. N {
			for j in 0 .. N {
				out.elems[i][j] = self.elems[j][i]
			}
		}
		out
	}

	pub fn contains_non_finite(&self) -> Option<(usize, usize)> {
		for i in 0 .. N {
			for j in 0 .. N {
				if !self.elems[i][j].is_finite() {
					return Some((i, j))
				}
			}
		}
		None
	}

	pub fn norm_sq(&self) -> f64 {
		let mut out = 0.0;
		for i in 0 .. N {
			for j in 0 .. N {
				out += self.elems[i][j].powi(2)
			}
		}
		out
	}
}

impl<T: Display, const N: usize> Display for SqMat<T, N> {
	fn fmt(&self, f: &mut Formatter<'_>) -> Result {
		write!(f, "(")?;
		for i in 0 .. N {
			if i != 0 {
				write!(f, "\n ")?
			}
			for j in 0 .. N - 1 {
				write!(f, "{}, ", self.elems[i][j])?
			}
			write!(f, "{}", self.elems[i][N - 1])?
		}
		write!(f, ")")?;
		Ok(())
	}
}

impl<const N: usize> Mul<[f64; N]> for SqMat<f64, N> {
	type Output = [f64; N];

	fn mul(self, rhs: [f64; N]) -> Self::Output {
		let mut out = [0.0; N];
		for i in 0 .. N {
			out[i] = dot(self.elems[i], rhs)
		}
		out
	}
}

impl<const N: usize> Mul<f64> for SqMat<f64, N> {
	type Output = Self;

	fn mul(self, rhs: f64) -> Self::Output {
		let mut out = SqMat::zero();
		for i in 0 .. N {
			for j in 0 .. N {
				out.elems[i][j] = self.elems[i][j] * rhs
			}
		}
		out
	}
}

impl<const N: usize> SubAssign for SqMat<f64, N> {
	fn sub_assign(&mut self, other: Self) {
		for i in 0 .. N {
			for j in 0 .. N {
				self.elems[i][j] -= other.elems[i][j]
			}
		}
	}
}

pub fn zip<F, T, U, const N: usize>(f: F, xs: [T; N], ys: [T; N]) -> [U; N]
where F: Fn(T, T) -> U, T: Copy, U: Copy + Default {
	let mut out = [Default::default(); N];
	for i in 0 .. N {
		out[i] = f(xs[i], ys[i])
	}
	out
}

pub fn zipf<F, T, const N: usize>(f: F, init: T, xs: [T; N], ys: [T; N]) -> T
where F: Fn(T, (T, T)) -> T, T: Copy {
	let mut out = init;
	for i in 0 .. N {
		out = f(out, (xs[i], ys[i]))
	}
	out
}

pub fn dot<const N: usize>(xs: [f64; N], ys: [f64; N]) -> f64 {
	let f = |a: f64, xy: (f64, f64)| a + xy.0 * xy.1;
	zipf(f, 0.0, xs, ys)
}

pub fn outer<const N: usize>(xs: [f64; N], ys: [f64; N]) -> SqMat<f64, N> {
	let mut out = SqMat::zero();
	for i in 0 .. N {
		for j in 0 .. N {
			out.elems[i][j] = xs[i] * ys[j]
		}
	};
	out
}

pub fn scalar_mul<const N: usize>(xs: [f64; N], y: f64) -> [f64; N] {
	let mut out = [0.0; N];
	for i in 0 .. N {
		out[i] = xs[i] * y
	};
	out
}

pub fn vec_sub<const N: usize>(xs: [f64; N], ys: [f64; N]) -> [f64; N] {
	zip(Sub::sub, xs, ys)
}

pub fn const_map<F, T, U, const N: usize>(f: F, xs: [T; N]) -> [U; N]
where F: Fn(T) -> U, T: Copy, U: Copy + Default {
	let mut out = [Default::default(); N];
	for i in 0 .. N {
		out[i] = f(xs[i])
	};
	out
}

pub fn kill_np(x: f64, y: f64) -> f64 {
	if x > 0.0 {
		y
	}
	else {
		0.0
	}
}

pub fn from_dist<D: Distribution<f64>, const N: usize>(dist: &D) -> [f64; N] {
	let mut out = [0.0; N];
	let mut rng = rand::thread_rng();
	for i in 0 .. N {
		out[i] = dist.sample(&mut rng)
	}
	out
}
