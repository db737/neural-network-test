use crate::activation::Activation;
use crate::util::*;
use rand_distr::Distribution;
use std::fmt::{Display, Formatter, Result};
use std::ops::Mul;

// `N` neurons per layer, in equally-sized layers (not counting the output
// layer, which has a single neuron). Intended for narrow (but possibly deep)
// networks, i.e. for `N` small.
#[derive(Clone, Debug)]
pub struct NeuralNetwork<T, AM, AO, const N: usize>
where AM: Activation<T>, AO: Activation<T>  {
	a_main: AM,
	a_output: AO,
	// Matrix element (i, j) is the weight neuron i has for neuron j in the
	// previous layer
	pub main_layers: Vec<SqMat<T, N>>,
	pub output_layer: [T; N],
	layers: usize
}

impl<AM, AO, const N: usize> NeuralNetwork<f64, AM, AO, N>
where AM: Activation<f64>, AO: Activation<f64> {
	pub fn new<D>(a_main: AM, a_output: AO, dist: &D, layers: usize) -> Self
	where D: Distribution<f64> + Clone {
		Self {
			a_main,
			a_output,
			main_layers: vec![SqMat::from_dist(dist); layers],
			output_layer: from_dist(dist),
			layers
		}
	}

	fn eval_layer(&self, is: [f64; N], i: usize) -> [f64; N] {
		const_map(|x: f64| self.a_main.eval(x), self.main_layers[i] * is)
	}

	pub fn eval(&self, is: [f64; N]) -> f64 {
		let mut out = is;
		for i in 0 .. self.layers {
			out = self.eval_layer(out, i)
		}
		self.a_output.eval(dot(self.output_layer, out))
	}

	// Train the network using this input/output pair
	pub fn learn(&mut self, is: [f64; N], out: f64, rate: f64) {
		let mut vals = vec![[0.0; N]; self.layers];
		vals[0] = is;
		// Forward-propagate to obtain the inputs for each layer
		for i in 0 .. self.layers - 1 {
			vals[i + 1] = self.eval_layer(vals[i], i)
		}
		let os = self.eval_layer(vals[self.layers - 1], self.layers - 1);
		let inp = dot(self.output_layer, os);
		// Descend the gradient faster based on how large the error currently is,
		// equivalent to least-squares
		let true_rate = (self.a_output.eval(inp) - out) * rate;
		// Train the output layer
		let d = self.a_output.deriv(inp);
		let mut derivas = scalar_mul(self.output_layer, d);
		let diffs = scalar_mul(os, d * true_rate);
		self.output_layer = vec_sub(self.output_layer, diffs);
		// Train the rest of the network
		for n0 in 0 .. self.layers {
			let n = self.layers - n0 - 1;
			let deriv = |x: f64| self.a_main.deriv(x);
			let ds = const_map(deriv, self.main_layers[n] * vals[n]);
			let derivs = zip(Mul::mul, ds, derivas);
			let diffss = outer(derivs, vals[n]) * true_rate;
			self.main_layers[n] -= diffss;
			derivas = self.main_layers[n].transpose() * derivs
		}
	}

	pub fn panic_on_non_finite(&self) {
		for i in 0 .. N {
			if !self.output_layer[i].is_finite() {
				panic!("Output layer is not finite: {:?}", self.output_layer)
			}
		}
		for (i, layer) in self.main_layers.iter().enumerate() {
			if let Some(_) = layer.contains_non_finite() {
				panic!("Layer {} is not finite: {}", i, layer)
			}
		}
	}
}

impl<T, AM, AO, const N: usize> Display for NeuralNetwork<T, AM, AO, N>
where T: Display, AM: Activation<T>, AO: Activation<T> {
	fn fmt(&self, f: &mut Formatter) -> Result {
		writeln!(f, "Main activation function: {:?}", self.a_main)?;
		writeln!(f, "Output activation function: {:?}", self.a_output)?;
		writeln!(f, "Weight matrices:")?;
		for (i, layer) in self.main_layers.iter().enumerate() {
			writeln!(f, "layer {}: ", i)?;
			writeln!(f, "{}", layer)?
		};
		write!(f, "Output layer weights: (")?;
		for i in 0 .. N - 1 {
			write!(f, "{}, ", self.output_layer[i])?
		};
		write!(f, "{})", self.output_layer[N - 1])?;
		Ok(())
	}
}
