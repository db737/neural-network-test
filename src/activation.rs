use std::fmt::Debug;
use crate::util::*;

pub trait Activation<T>: Copy + Debug {
	fn eval(&self, x: T) -> T;

	fn deriv(&self, x: T) -> T;
}

#[derive(Clone, Copy, Debug)]
pub struct Logistic {}

impl Logistic {
	pub fn new() -> Self {
		Self {}
	}
}

impl Activation<f64> for Logistic {
	fn eval(&self, x: f64) -> f64 {
		1.0 / (1.0 + (-x).exp())
	}

	fn deriv(&self, x: f64) -> f64 {
		let exp = (-x).exp();
		exp * (1.0 + exp).powi(-2)
	}
}

#[derive(Clone, Copy, Debug)]
pub struct Tanh {}

impl Tanh {
	pub fn new() -> Self {
		Self {}
	}
}

impl Activation<f64> for Tanh {
	fn eval(&self, x: f64) -> f64 {
		x.tanh()
	}

	fn deriv(&self, x: f64) -> f64 {
		x.cosh().powi(-2)
	}
}

#[derive(Clone, Copy, Debug)]
pub struct SoftPlus {}

impl SoftPlus {
	pub fn new() -> Self {
		Self {}
	}
}

impl Activation<f64> for SoftPlus {
	fn eval(&self, x: f64) -> f64 {
		(1.0 + x.exp()).ln()
	}

	fn deriv(&self, x: f64) -> f64 {
		Logistic::new().eval(x)
	}
}

#[derive(Clone, Copy, Debug)]
pub struct SmoothLU {}

impl SmoothLU {
	pub fn new() -> Self {
		Self {}
	}
}

impl Activation<f64> for SmoothLU {
	fn eval(&self, x: f64) -> f64 {
		kill_np(x, x * (-1.0 / x).exp())
	}

	fn deriv(&self, x: f64) -> f64 {
		let pm1 = -1.0 / x;
		kill_np(x, (1.0 - pm1) * pm1.exp())
	}
}

#[derive(Clone, Copy, Debug)]
pub struct Linear {
	pub a: f64
}

impl Linear {
	pub fn new(a: f64) -> Self {
		Self {
			a
		}
	}
}

impl Activation<f64> for Linear {
	fn eval(&self, x: f64) -> f64 {
		self.a * x
	}

	fn deriv(&self, _: f64) -> f64 {
		self.a
	}
}

#[derive(Clone, Copy, Debug)]
pub struct LeakyRELU {
	pub am: f64
}

impl LeakyRELU {
	pub fn new(am: f64) -> Self {
		Self {
			am
		}
	}
}

impl Activation<f64> for LeakyRELU {
	fn eval(&self, x: f64) -> f64 {
		if x >= 0.0 {
			x
		}
		else {
			self.am * x
		}
	}

	fn deriv(&self, x: f64) -> f64 {
		if x >= 0.0 {
			1.0
		}
		else {
			self.am
		}
	}
}
