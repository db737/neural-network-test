extern crate rand;
extern crate rand_distr;

pub mod activation;
pub mod neural_network;
pub mod util;

use rand_distr::*;
use activation::*;
use neural_network::*;
use std::io::Write;
use std::fs::File;
use std::panic::catch_unwind;

fn main() {
	let a_main = Tanh::new();
	let a_output = Logistic::new();
	let dist = Normal::new(0.0, 0.3).unwrap();
	let mut nn = NeuralNetwork::new(a_main, a_output, &dist, 5);
	let mut prev = nn.clone();
	let rate = 0.01;
	println!("Training");
	let nt = 1000;
	let f = |x: usize, nmax: usize| {
		let nf = nmax as f64;
		(x as f64 - 0.5 * nf) * 4.0 / nf
	};
	let h = |x: f64, y: f64| -> f64 {
		let out = (x * y).tanh();
		Logistic::new().eval(out)
	};
	let step = (nt as f64).powi(2) / 100.0;
	let mut prevc = 0.0;
	let mut count = 0;
	for i in 0 .. nt {
		for j in 0 .. nt {
			let (x, y) = (f(i, nt), f(j, nt));
			nn.learn([x, y, 1.0], h(x, y), rate);
			match catch_unwind(|| nn.panic_on_non_finite()) {
				Ok(_) => prev = nn.clone(),
				Err(_) => panic!("{}", prev)
			}
			if (i * nt + j) as f64 >= prevc {
				if count % 10 == 0 {
					print!("\n{}% done ", (prevc / step + 0.5).floor() as u8)
				}
				else {
					print!(".")
				}
				count += 1;
				prevc += step
			}
		}
	}
	println!("\n\nTrained");
	let mut netout = File::create("data/nn.txt").unwrap();
	writeln!(netout, "{}", nn).unwrap();
	let ne = 40;
	let mut dataout = File::create("data/out.csv").unwrap();
	for i in 0 .. ne {
		for j in 0 .. ne {
			let (x, y) = (f(i, ne), f(j, ne));
			let out = nn.eval([x, y, 1.0]);
			writeln!(dataout, "{}, {}, {}, {}", x, y, out, h(x, y)).unwrap()
		}
	}
}
