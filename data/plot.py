import numpy as np
import matplotlib.pyplot as plt

xyzs = np.loadtxt("data/out.csv", delimiter = ", ")
n = np.sqrt(xyzs.shape[0]).astype(int)
xxs = xyzs[:, 0].reshape(n, n)
yys = xyzs[:, 1].reshape(n, n)
zz0s = xyzs[:, 2].reshape(n, n)
zz1s = xyzs[:, 3].reshape(n, n)
fig, axs = plt.subplots(1, 2)
axs[0].contourf(xxs, yys, zz0s)
r2r2s = xxs * xxs + yys * yys
axs[1].contourf(xxs, yys, zz1s)
plt.show()
